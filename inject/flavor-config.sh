#!/bin/bash -x

source /host/settings.sh

config-cinnamon() {
    # set the correct x-session-manager
    update-alternatives --set x-session-manager /usr/bin/cinnamon-session

    # suppress the warning 'Running in software rendering mode' of cinnamon
    echo 'export CINNAMON_2D=true' > /etc/X11/Xsession.d/99cinnamon2d

    # script for session logout
    cat <<EOF > /usr/local/bin/session-logout.sh
#!/bin/bash
cinnamon-session-quit --no-prompt --force --logout
EOF
    chmod +x /usr/local/bin/session-logout.sh
}

config-mate() {
    # set the correct x-session-manager
    update-alternatives --set x-session-manager /usr/bin/mate-session

    # script for session logout
    cat <<EOF > /usr/local/bin/session-logout.sh
#!/bin/bash
mate-session-save --force-logout
EOF
    chmod +x /usr/local/bin/session-logout.sh
}

config-xfce() {
    # set the correct x-session-manager
    update-alternatives --set x-session-manager /usr/bin/xfce4-session

    # script for session logout
    cat <<EOF > /usr/local/bin/session-logout.sh
#!/bin/bash
xfce4-session-logout --logout
EOF
    chmod +x /usr/local/bin/session-logout.sh
}

# call the correct config function
config-$FLAVOR
