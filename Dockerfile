include(jammy)

include(dockerfile/unminimize-ubuntu)
include(dockerfile/add-linuxmint-repo)

include(dockerfile/install-xrdp)
include(dockerfile/misc)

### install the desktop
define(FLAVOR,`esyscmd(`printf ${FLAVOR:-mate}')')dnl
RUN DEBIAN_FRONTEND=noninteractive \
    apt install --yes -o Dpkg::Options::=--force-confnew --install-recommends \
        mint-meta-FLAVOR
RUN systemctl set-default graphical.target

### install additional packages
sinclude(packages)
