cmd_users_help() {
    cat <<_EOF
    users [<command>] [<filename>]
        Run 'ds inject users.sh [<command>] [<filename>]'
        Where <command> can be: 
            help | create | create-guests | remove-guests
            | export | import | backup | restore

_EOF
}

cmd_users() {
    set -x
    ds inject users.sh "$@"
}
